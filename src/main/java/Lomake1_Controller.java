import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.event.ActionEvent;
import java.io.File;
import javafx.scene.image.*;


public class Lomake1_Controller {
    private String sijainti;

    @FXML
    private TextField nimi;

    @FXML
    private TextField osoite;

    @FXML
    private TextField rakennusvuosi;

    @FXML
    private TextField neliomaara;

    @FXML
    private TextField vuokra;

    @FXML
    private ChoiceBox<?> kuntoluokitus;

    @FXML
    private ImageView kuva;

    @FXML
    private Button lisaaOmaKuva;

    @FXML
    void lisaaOmaKuvaAction() {
        valitseKuva();
    }

    @FXML
    private TextArea kuvaus_kohteesta;

    @FXML
    private TextField muut_ehdot;

    @FXML
    private TextField email;

    @FXML
    private Button tallenna_jatka;

    @FXML //user inputit validoidaan ennen "tallentamista"
    void tallenna_jatkaAction() {

        if (onkoInteger(rakennusvuosi) == true) {
            if (onkoFloat(neliomaara) == true) {
                if (onkoDouble(vuokra) == true) {
                    Asunto asunto= new Asunto();
                    asunto.setNimi(nimi.getText());
                    asunto.setOsoite(osoite.getText());

                    asunto.setRakennusvuosi(Integer.parseInt(rakennusvuosi.getText()));
                    asunto.setNeliömäärä(Float.parseFloat(neliomaara.getText()));
                    asunto.setVuokra(Double.parseDouble(vuokra.getText()));

                    String[] kuvaus=new String[]{kuvaus_kohteesta.getText()};
                    asunto.setKohteenKuvaus(kuvaus);
                    asunto.setMuutEhdot(muut_ehdot.getText());
                    asunto.setSähköposti(email.getText());

                    asunto.setKuvaTiedosto(sijainti);
                    AsuntoGeneraattori.lisaaListaan(asunto);
                    System.out.println(AsuntoGeneraattori.annaAsunnot().toString()); //testitulostus

                    Stage kehys = (Stage) tallenna_jatka.getScene().getWindow();
                    kehys.close();
                }
            }
        }
    } //tallenna_jatkaAction

    @FXML
    private Button peruuta;

    @FXML
    void peruutaAction() {
        Main.setAppState(Main.AppState.MainMenu);
    }

    //Validointia rakennusvuodelle
    private boolean onkoInteger(TextField rakennusvuosi) {
        try{
            int vuosi = Integer.parseInt(rakennusvuosi.getText());
            return true;
        } catch (NumberFormatException e){
            System.out.println("Anna rakennusvuosi kokonaislukuna pls");
            return false;
        }
    }
    //Validoidaan neliomaara
    private boolean onkoFloat(TextField neliomaara) {
        try {
            float koko = Float.parseFloat(neliomaara.getText());
            return true;
        } catch (NumberFormatException e) {
            System.out.println("Anna koko floattina kiitos");
            return false;
        }
    }

    //validoidaan vuokra
    private boolean onkoDouble(TextField vuokra) {
        try{
            double kkvuokra = Double.parseDouble(vuokra.getText());
            return true;
        }catch (NumberFormatException e) {
            //e.printStackTrace();
            System.out.println("Anna vuokra doublena");
            return false;
        }
    }
    //kuvan valitseminen
    private void valitseKuva() {
        FileChooser valitsija = new FileChooser();
        //Filteröidään pois kaikki muut kuin .png ja .jpg filut
        valitsija.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Tiedostotyypit", "*.png", "*.jpg"));
        valitsija.setTitle("Avaa kuvatiedosto");
        File filu = valitsija.showOpenDialog(new Stage());
        if (filu != null) {
            sijainti = (filu.getAbsoluteFile().toURI().toString());
            Image img = new Image(sijainti);
            kuva.setImage(img);
        }
    }//valitseKuva


}
