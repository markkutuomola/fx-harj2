import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import java.util.ArrayList;

public class Lomake2_Controller {


    @FXML
    private TextField vuosiAlku;

    @FXML
    private TextField kokoAlku;

    @FXML
    private TextField vuokraAlku;

    @FXML
    private Button haeAsuntoja;

    @FXML
    private TextField vuosiLoppu;

    @FXML
    private TextField kokoLoppu;

    @FXML
    private TextField vuokraLoppu;

    @FXML
    private TilePane hakuTulokset;

    @FXML
    private Button palaaValikkoon;

    @FXML
    private void palaaValikkoonAction() {
        Main.setAppState(Main.AppState.MainMenu);
    }

    @FXML
    private ImageView kuva;

    @FXML
    private void haeAsuntojaAction() {
        updateList();
    }

    @FXML
    private Button Rnd;

    @FXML
    private ProgressBar palkki;


    @FXML
    private TextField loytyneitaAsuntoja;

    @FXML
    private void RndAction() {
        AsuntoGeneraattori.luoAsuntoja(50); //luo satunnaisasuntoja
    }

    public void initialize(){
        //kuuntelija vuokraLoppuun, muutos päivittää näytetyt kohteet
        vuokraLoppu.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                updateList();
            }
        });

        vuokraAlku.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                updateList();
            }
        });

        AsuntoGeneraattori.lista.addListener(new ListChangeListener<Asunto>() {
            @Override
            public void onChanged(Change<? extends Asunto> change) {
                palkki.setProgress((double)AsuntoGeneraattori.tehtyjäAsuntoja()/AsuntoGeneraattori.tehtäviäAsuntoja());
                updateList();
            }
        });
    }

    public void updateList() {

            Platform.runLater(new Runnable(){
                int rvuosi;
                float koko;
                double vuokra;
                int loytyneita=0;
                public void run() {
                    hakuTulokset.getChildren().clear();

                    ArrayList<Asunto> asunnot = AsuntoGeneraattori.annaAsunnot();

                    for(int i = 0; i<asunnot.size(); i++){

                        rvuosi = asunnot.get(i).getRakennusvuosi();
                        koko = asunnot.get(i).getNeliömäärä();
                        vuokra = asunnot.get(i).getVuokra();

                        if (vuokra >= Double.parseDouble(vuokraAlku.getText()) && vuokra <= Double.parseDouble(vuokraLoppu.getText())) {
                            loytyneita++;
                            Label vuosi = new Label("Rakennusvuosi: " + (rvuosi));
                            Label size = new Label("Koko: " + (koko) + "m^2");
                            Label rent = new Label("Vuokra: " + (vuokra) + "€/kk");

                            hakuTulokset.getChildren().addAll(vuosi, size, rent);
                            loytyneitaAsuntoja.setText(Integer.toString(loytyneita));
                        }
                    }
                }
            });
    }
}
