import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.util.Duration;
import java.time.*;
import java.time.format.*;




public class Main extends Application {

    private Stage kehys;
    private Button nappi;
    private Button nappi2;
    private Button nappi3;
    private Label laapeli;


    public enum AppState {AddingProperty, SearchingProperty, MainMenu, Exit}

    private static ObjectProperty<AppState> op = new SimpleObjectProperty<>();

    //ehkä turha
    private static AppState getAppState() {
        return op.get();
    }

    public static void setAppState(AppState newop) {
        op.set(newop);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        op.addListener((observable, oldValue, newValue) ->
                {
                   if (newValue == AppState.AddingProperty) {
                       try{

                           kehys = new Stage();
                           kehys.initModality(Modality.APPLICATION_MODAL); //prevents using the menu window
                           kehys.setTitle("Lisää vuokra-asunto");
                           kehys.setScene(new Scene(FXMLLoader.load(getClass().getResource("lomake1.fxml")), 600, 800));
                           kehys.show();}

                       catch(IOException io) {
                           io.printStackTrace();
                       }
                   }
                   else if (newValue == AppState.SearchingProperty) {
                       try{
                           kehys = new Stage();
                           kehys.initModality(Modality.APPLICATION_MODAL);
                           kehys.setResizable(false);
                           kehys.setTitle("Hae vuokra-asuntoja");
                           kehys.setScene(new Scene(FXMLLoader.load(getClass().getResource("lomake2_v2.fxml")), 439, 460));
                           kehys.show();}
                       catch(IOException io) {
                           io.printStackTrace();
                       }
                   }
                   else if (newValue == AppState.MainMenu) {
                       kehys.close();
                   }
                   //Appstate.Exit, poistutaan ohjelmasta
                   else {
                       System.exit(0);
                   }

                });



        primaryStage.setTitle("Valitse lomake");

        //lomake 1 avaaminen uuteen ikkunaan
        nappi = new Button("Lomake 1");
        nappi.setOnAction(e -> {
            setAppState(AppState.AddingProperty);
        });

        //lomake 2 avaaminen uuteen ikkunaan
        nappi2 = new Button("Lomake 2");
        nappi2.setOnAction(e -> {
            setAppState(AppState.SearchingProperty);
        });

        nappi3 = new Button("Exit");
        nappi3.setOnAction(e -> {
            setAppState(AppState.Exit);
        });

        DateTimeFormatter dtf =DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        laapeli = new Label(dtf.format(LocalDateTime.now()));

        Timeline timeline= new Timeline(new KeyFrame(Duration.millis(2000), evt -> laapeli.setText(":DDD ebin")),
                new KeyFrame(Duration.millis(1000), evt -> laapeli.setText(dtf.format(LocalDateTime.now()))));

        timeline.setCycleCount(Animation.INDEFINITE);

        timeline.play();

        VBox layout = new VBox(5);
        layout.getChildren().addAll(nappi, nappi2, nappi3, laapeli);
        layout.setAlignment(Pos.CENTER);

        Scene scene1 = new Scene(layout,200,125);
        primaryStage.setScene(scene1);
        primaryStage.show();

    }//start

    public static void main(String[] args) {

        AsuntoGeneraattori.luoAsuntoja(2); //Luodaan 2 kpl testiasuntoja
        launch(args);

    }//main

}
